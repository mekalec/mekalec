# README #

I came home from work one night and the wife wants to know what I want to eat tonight.
I didn't know what I wanted to eat nor did she. So, I opened our app called WIW2E
to check and see what we wanted to eat. We decided on Chinese. 
The app searched our kitchen to find ingredients for Chinese dishes and
we settled on Chinese Dumplings/

Problem: To lessen the indecisiveness when it comes to choosing meals to cook each
night.

Solution: WIW2E - An app that searches the kitchen for ingredients based on what is 
inside the kitchen.

### What is this repository for? ###

This respository is for the judges of the KCFed Code-a-thon to have easy access tour program.

### How do I get set up? ###
Go to the source file and run it.
Make sure the pictures and ingredients are in the right file to run it.
### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact